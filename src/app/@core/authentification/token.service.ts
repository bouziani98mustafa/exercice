import { Injectable } from '@angular/core';
const TOKEN_KEY = 'jwtToken';
@Injectable({ providedIn: 'root' })
export class TokenService {
  getToken(): String {
    return window.localStorage[TOKEN_KEY];
  }

  saveToken(token: String) {
    window.localStorage[TOKEN_KEY] = token;
  }

  destroyToken() {
    window.localStorage.removeItem(TOKEN_KEY);
  }
}
