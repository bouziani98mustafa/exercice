import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenService } from '../authentification/token.service';
import { Logger } from '../logger.service';
const log = new Logger('interceptor');
const headersConfig = {
  'Content-Type': 'application/json',
  Accept: 'application/json'
};

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public jwtService: TokenService) {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    console.error("Intercepted ....")

    const token = this.jwtService.getToken();
    const excludeUrls = ['internal/entities/front/']; //url

    if (token && !excludeUrls.some((e) => req.url.includes(e))) {
      headersConfig['Authorization'] = `Bearer ${token}`;
      req = req.clone({ setHeaders: headersConfig });
    }

    return next.handle(req);
  }
}
