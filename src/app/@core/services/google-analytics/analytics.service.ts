import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Logger } from '@core/logger.service';
import { environment } from '@env/environment';

declare var gtag: Function;
declare const ga: any;

const log = new Logger('AnalyticsService');

@Injectable({
  providedIn: 'root',
})
export class AnalyticsService {
  constructor(private router: Router) {}

  public event(eventName: string, params: {}) {
    gtag('event', eventName, params);
  }

  public init() {
    this.listenForRouteChanges();
    //
    this.appendGaTrackingCode();
  }

  private listenForRouteChanges() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // if you use appendgtagTrackingCode function  use gtag  & hide ga('send'....);
        // gtag('config', environment.googleAnalyticsKey, {
        //   page_path: event.urlAfterRedirects,
        // });

        ga('send', { hitType: 'pageview', page: event.urlAfterRedirects });

        log.info(
          'Sending Google Analytics hit for route',
          event.urlAfterRedirects
        );
        log.info('Property ID', environment.googleAnalyticsKey);
      }
    });
  }

  // the new recommendation of google analytic using gtag
  //https://developers.google.com/analytics/devguides/collection/upgrade/analyticsjs
  appendgtagTrackingCode() {
    try {
      const script1 = document.createElement('script');
      script1.async = true;
      script1.src =
        'https://www.googletagmanager.com/gtag/js?id=' +
        environment.googleAnalyticsKey;
      document.head.appendChild(script1);

      const script2 = document.createElement('script');

      script2.innerHTML =
        `
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', '` +
        environment.googleAnalyticsKey +
        `', {'send_page_view': false});
    `;
      document.head.appendChild(script2);
    } catch (ex) {
      log.error('Error appending google analytics');
      log.error(ex);
    }
  }
  // old config
  appendGaTrackingCode() {
    try {
      const script = document.createElement('script');
      script.innerHTML = `
      (function (window) {
        /* Google Analytics pour recette externe uniquement */
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga(
          'create',
         ${environment.production ? '"UA-104548710-1"' : '"UA-104548710-2"'},
        ${environment.production ? '"auto"' : '"none"'}
        );
      }(this))
    `;

      document.head.appendChild(script);
    } catch (ex) {
      console.error('Error appending google analytics');
      console.error(ex);
    }
  }
}
