import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MailBoxesComponent} from "@modules/mail-boxes/mail-boxes/mail-boxes.component";

const routes: Routes = [{ path: '', component: MailBoxesComponent }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MailBoxesRoutingModule { }
