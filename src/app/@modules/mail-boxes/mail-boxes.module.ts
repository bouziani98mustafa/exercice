import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MailBoxesComponent } from './mail-boxes/mail-boxes.component';
import {HeaderComponent} from "@shared/header/header.component";
import {OverviewSchemaComponent, SharedModule} from "@shared";
import {FooterComponent} from "@shared/footer/footer.component";
import {RouterModule} from "@angular/router";
import {MailBoxesRoutingModule} from "@modules/mail-boxes/mail-boxes-routing.module";
import { FormsModule } from '@angular/forms';
import { NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  declarations: [MailBoxesComponent],
  imports: [
    CommonModule,
    MailBoxesRoutingModule,
    SharedModule.forRoot(),
    RouterModule,
    FormsModule,
    NgbTabsetModule,
    TranslateModule
  ],
  exports: [
    MailBoxesComponent
  ],
})
export class MailBoxesModule { }
