import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailBoxesComponent } from './mail-boxes.component';

describe('MailBoxesComponent', () => {
  let component: MailBoxesComponent;
  let fixture: ComponentFixture<MailBoxesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailBoxesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailBoxesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
