import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {row} from "@shared/table/table.model";
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-mail-boxes',
  templateUrl: './mail-boxes.component.html',
  styleUrls: ['./mail-boxes.component.scss']
})
export class MailBoxesComponent implements OnInit {

  columns :string[];

  rows :row[];
  @Input() submitted = false;
  user :row[] ;
  closeResult = '' ;
  // les trois champs variable de binding
  adress :string ;
  etat :boolean ;
  frontal :string;
  // des variables pour la surbriance
  @Input() searched = new EventEmitter<string>();
  public Search: string = null;
  @Input() surbriance : boolean =true ;

  constructor(private route: ActivatedRoute,
              private router: Router,private modalService: NgbModal) {
  }

  ngOnInit() {
    this.loadTable();
  }

  loadTable(){
    this.rows =  [
       {rowId: 1,
         data : [true,true,'production','1assurance@01.rss.fr','relai@mail.com','acceuil','500 000','superMail@mail.com','123.159.456.789'],
        subrow :[
             [true,'assurance@01.rss.fr','relai@mail.com','acceuil','500 000','superMail@mail.com','123.159.456.789'],
             [true,'assurance@01.rss.fr','relai@mail.com','acceuil','500 000','superMail@mail.com','123.159.456.789']
        ]
       },
        {
          rowId: 2,
          data : [true,false,'production','2assurance@01.rss.fr','relai@mail.com','acceuil','500 000','superMail@mail.com','123.159.456.789'],
          subrow :[
            [true,'assurance@01.rss.fr','relai@mail.com','acceuil','500 000','superMail@mail.com','123.159.456.789'],
            [true,'assurance@01.rss.fr','relai@mail.com','acceuil','500 000','superMail@mail.com','123.159.456.789']
          ]
        },
      {
        rowId: 3,
        data : [true,false,'production','3assurance@01.rss.fr','relai@mail.com','acceuil','500 000','superMail@mail.com','123.159.456.789'],
        subrow :[
          [true,'assurance@01.rss.fr','relai@mail.com','acceuil CSE','500 000','superMail@mail.com','123.159.456.789'],
          [true,'assurance@01.rss.fr','relai@mail.com','acceuil CSE','500 000','superMail@mail.com','123.159.456.789']
        ]
      },
      {
        rowId: 4,
        data : [false, true,'service','ddrame@01.rss.fr','relai@mail.com','acceuil','500 000','superMail@mail.com','123.159.456.789'],
        subrow : [[false,'assurance@01.rss.fr','relai@mail.com','acceuil CSE','500 000','superMail@mail.com','123.159.456.789'],
        [true,'assurance@01.rss.fr','relai@mail.com','acceuil CSE','500 000','superMail@mail.com','123.159.456.789']
      ]},
      {
        rowId: 5,
        data : [true, true,'test','rtosgdd@01.rss.fr','relai@mail.com','frontal AB','500 000','superMail@mail.com','123.159.456.789'],
        subrow :[ [true,'assurance@01.rss.fr','relai@mail.com','frontal AB','500 000','superMail@mail.com','123.159.456.789'],
          [true,'assurance@01.rss.fr','relai@mail.com','acceuil CSE','500 000','superMail@mail.com','123.159.456.789']
    ]
      },
      {
        rowId: 6,
        data : [false, true,'test','SoftEmbeded@01.rss.fr','relai@mail.com','acceuil','500 000','superMail@mail.com','123.159.456.789'],
        subrow : [true,'assurance@01.rss.fr','relai@mail.com','frontal 1','500 000','superMail@mail.com','123.159.456.789']
      },
      {
        rowId: 7,
        data : [false, true,'test','SoftEmbeded@01.rss.fr','relai@mail.com','acceuil','500 000','superMail@mail.com','123.159.456.789'],
        subrow : [true,'assurance@01.rss.fr','relai@mail.com','frontal 1','500 000','superMail@mail.com','123.159.456.789']
      },
        {
          rowId: 8,
          data : [true, true,'production','7assurance@01.rss.fr','assurance@mail.com','frontal AB','500 000','superMail@mail.com','123.159.456.789'],
          subrow :[
            [true,'assurance@01.rss.fr','relai@mail.com','acceuil CSE','500 000','superMail@mail.com','123.159.456.789'],
            [true,'assurance@01.rss.fr','relai@mail.com','acceuil CSE','500 000','superMail@mail.com','123.159.456.789']
          ]
        }
    ];

    this.columns =  ['etat','Rejouable','Type','Adresse','Relais de messagerie','Frontal','Seuil(Ko)','Email d\'alerte','IPs'];
  }




  /* filtre fonction filtre statique filtre dans le front qui n'a pas important le filtre est un appelle api
  * je test sur les 3 champs etat et adress et frontal qui sont n'est pas vides apres je fait le traitement en
  * comparant les champs avec data et les filtres
  *  */
  filtrer(): void{
    const rowsOld :row[] =this.rows;
    const rowsFound :row[] =[];
    if(this.adress!==undefined  && this.etat!==undefined && this.frontal!==undefined){
      for(let row of this.rows){
        this.rows.filter((row) => row.data[0] ==this.etat && (row.data[5]).startsWith(this.frontal)  );
        if((this.etat=== row.data[0]) &&(row.data[5]).startsWith(this.frontal) && (row.data[3].indexOf(this.adress)!==-1) && (this.etat=== row.data[0])){
          rowsFound.push(row);
        } else if((row.data[3].indexOf(this.adress)!==-1) && (row.data[0]==this.etat)){
          rowsFound.push(row);
        }
        else if((row.data[3].indexOf(this.adress)!==-1) && (row.data[5]).startsWith(this.frontal)==true){
          rowsFound.push(row);
          console.log("done");
        } else{
          this.rows=rowsOld ;
        }
      }
      this.rows=rowsFound ;
    } else if(this.adress!==undefined  && this.etat!==undefined){
      for(let row of this.rows){
        console.log((row.data[5]).startsWith(this.frontal))
        if((row.data[3].indexOf(this.adress)!==-1) && (row.data[0]==this.etat)){
          rowsFound.push(row);
        }
        else if((row.data[3].indexOf(this.adress)!==-1) && (row.data[0]!==this.etat)){
          rowsFound.push(row);
        }
      }
      this.rows=rowsFound ;
    }  else if(this.adress!==undefined  && this.frontal!==undefined){
      for(let row of this.rows){
        if((row.data[3].indexOf(this.adress)!==-1) && (row.data[0].startsWith(this.frontal))){
          rowsFound.push(row);
        }
        else{
          this.rows=rowsOld ;
        }
      }
      this.rows=rowsFound ;
    }  else if(this.etat!==undefined  && this.frontal!==undefined){
      for(let row of this.rows){
        if((row.data[3]==this.etat) && (row.data[0].startsWith(this.frontal))){
          rowsFound.push(row);
        }
        else if((row.data[3].indexOf(this.adress)!==-1) && (row.data[0]!==this.frontal)){
          rowsFound.push(row);
        } else{
          this.rows=rowsOld ;
        }
      }
      this.rows=rowsFound ;
    }
    else if(this.etat!==undefined){
      const rowsFound :row[] =[];
      for(let row of this.rows){
        if(this.etat == (row.data[0])){
          rowsFound.push(row);
          console.error("filtre with etat fonctionne")
        }
      }
      this.rows=rowsFound ;
    }
    else{
      this.rows = rowsOld;
      this.loadTable();
    }
  }

  /*
  ces deux fonction c'est pour la surbriance dans la bare de filre d'adress
   */
  public OnSearched(searchTerm: string) {
    this.adress = searchTerm;
  }
  public onSearch(searchTerm: string): void {
    this.searched.emit(searchTerm);
  }

  effacer(){
    this.etat=true ;
    this.frontal="";
    this.adress="";
   // location.reload();

  }

  /*
  open c'est la fonction qui couvre le popup (le modal) qui existe dans la meme composant dans .html
  et on peut changer le size md ou xl  sd on peut aussi modifier son style avec cette class windowClass: 'custom-class'
  getDismissReason qui faire apparaitre le modal et le fermer
   */
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'MailBoxesComponent',size: 'md',backdrop:'static',
      windowClass: 'custom-class'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
