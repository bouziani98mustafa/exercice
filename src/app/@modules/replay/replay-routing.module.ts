import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UsersComponent} from "@modules/users/users/users.component";
import {ReplayComponent} from "@modules/replay/replay/replay.component";


const routes: Routes = [{ path: '', component: ReplayComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReplayRoutingModule { }
