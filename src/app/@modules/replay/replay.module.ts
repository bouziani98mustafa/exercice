import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReplayRoutingModule } from './replay-routing.module';
import { ReplayComponent } from './replay/replay.component';
import {HeaderComponent} from "@shared/header/header.component";
import {OverviewSchemaComponent} from "@shared";
import {FooterComponent} from "@shared/footer/footer.component";
import {RouterModule} from "@angular/router";


@NgModule({
  declarations: [ReplayComponent],
  imports: [
    CommonModule,
    ReplayRoutingModule,
    RouterModule
  ],
  exports: [
    ReplayComponent
  ],

})
export class ReplayModule { }
