import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MailBoxesComponent} from "@modules/mail-boxes/mail-boxes/mail-boxes.component";
import {UsersComponent} from "@modules/users/users/users.component";


const routes: Routes = [{ path: '', component: UsersComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
