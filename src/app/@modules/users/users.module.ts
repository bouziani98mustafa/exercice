import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users/users.component';
import {HeaderComponent} from "@shared/header/header.component";
import {OverviewSchemaComponent} from "@shared";
import {FooterComponent} from "@shared/footer/footer.component";
import {RouterModule} from "@angular/router";
import {UsersRoutingModule} from "@modules/users/users-routing.module";



@NgModule({
  declarations: [UsersComponent],
  imports: [
    CommonModule,
    RouterModule,
    UsersRoutingModule
  ],
  exports: [
    UsersComponent,
  ],
})
export class UsersModule { }
