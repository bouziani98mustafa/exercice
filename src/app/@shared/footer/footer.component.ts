import { Component, OnInit } from '@angular/core';
import { Lang } from '@app/@shared';
import { I18nService } from '@app/@i18n/i18n.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  supportedLangs: Array<Lang> = [];
  selectedLang: Lang;

  constructor(private i18nService: I18nService) {
    this.supportedLangs = this.i18nService.getSupportedLangs();
    this.selectedLang = this.supportedLangs[0];
  }

  ngOnInit() {}

  onLangChange(lang: Lang) {
    this.selectedLang = lang;
    this.i18nService.language = lang.langId;
  }
}
