import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Store } from '@ngrx/store';
import { selectAccountBasicInfo } from '@app/@store';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class HeaderComponent implements OnInit {
  isOpen = false;
  accountBasicInfo$;

  constructor(
    private modalService: NgbModal,
    public breakpointObserver: BreakpointObserver,

    private route: ActivatedRoute,
    private router: Router,
    private store: Store<any>
  ) {}
  ngOnInit() {
    // 768px : md
    this.breakpointObserver
      .observe(['(min-width: 768px)'])
      .subscribe((state: BreakpointState) => {
        if (state.matches && this.isOpen) {
          this.createModal().dismissAll();
          this.isOpen = !this.isOpen;
        }
      });

    this.accountBasicInfo$ = this.store.select(selectAccountBasicInfo);
  }

  createModal() {
    const modalRef = this.modalService;
    return modalRef;
  }
  toggle() {
  }
  logout() {

  }
}
