import { NodeChild } from './node-child';

export interface SchemaNode {
  label: string;
  childs: NodeChild[];
}
