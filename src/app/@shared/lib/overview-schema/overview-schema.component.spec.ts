import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverviewSchemaComponent } from './overview-schema.component';

fdescribe('OverviewSchemaComponent', () => {
  let component: OverviewSchemaComponent;
  let fixture: ComponentFixture<OverviewSchemaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OverviewSchemaComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverviewSchemaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
