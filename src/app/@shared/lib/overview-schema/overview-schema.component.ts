import { Component, OnInit, Input } from '@angular/core';
import { SchemaNode } from './models/schema-node';

@Component({
  selector: 'app-overview-schema',
  templateUrl: './overview-schema.component.html',
  styleUrls: ['./overview-schema.component.scss'],
})
export class OverviewSchemaComponent implements OnInit {
  @Input() nodes: SchemaNode[];

  constructor() {}

  ngOnInit() {}
}
