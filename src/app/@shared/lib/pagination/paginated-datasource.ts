import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import {
  Observable,
  Subject,
  BehaviorSubject,
  combineLatest,
  fromEvent,
} from 'rxjs';
import {
  switchMap,
  startWith,
  map,
  share,
  scan,
  concatMap,
  tap,
} from 'rxjs/operators';
import { indicate } from './operators';
import { Page, Sort, PaginatedEndpoint } from './page';

export interface Source<T> extends DataSource<T> {
  connect(collectionViewer?: CollectionViewer): Observable<T[]>;
  disconnect(): void;
}

export class PaginatedDataSource<T, Q> implements Source<T> {
  protected pageNumber = new Subject<number>();
  protected sort: BehaviorSubject<Sort<T>>;
  protected query: BehaviorSubject<Q>;
  protected loading = new Subject<boolean>();

  public loading$ = this.loading.asObservable();
  public page$: Observable<Page<T>>;

  constructor(
    protected endpoint: PaginatedEndpoint<T, Q>,
    initialSort: Sort<T>,
    initialQuery: Q,
    public pageSize = 20,
    public isInfinit = false
  ) {
    this.query = new BehaviorSubject<Q>(initialQuery);
    this.sort = new BehaviorSubject<Sort<T>>(initialSort);
    const param$ = combineLatest([this.query, this.sort]);
    this.page$ = param$.pipe(
      switchMap(([query, sort]) =>
        this.pageNumber.pipe(
          startWith(0),
          switchMap((page) =>
            this.endpoint({ page, sort, size: this.pageSize }, query).pipe(
              indicate(this.loading)
            )
          )
        )
      ),
      share()
    );
  }

  sortBy(sort: Partial<Sort<T>>): void {
    const lastSort = this.sort.getValue();
    const nextSort = { ...lastSort, ...sort };
    this.sort.next(nextSort);
  }

  queryBy(query: Partial<Q>): void {
    const lastQuery = this.query.getValue();
    const nextQuery = { ...lastQuery, ...query };
    this.query.next(nextQuery);
  }

  fetch(page: number): void {
    this.pageNumber.next(page);
  }

  connect(): Observable<T[]> {
    return this.page$.pipe(map((page) => page.content));
  }

  disconnect(): void {}
}
export class PaginatedScorollInfinit<T, Q> extends PaginatedDataSource<T, Q> {
  constructor(
    protected endpoint: PaginatedEndpoint<T, Q>,
    initialSort: Sort<T>,
    initialQuery: Q,
    public pageSize = 20
  ) {
    super(endpoint, initialSort, initialQuery, pageSize);

    const param$ = combineLatest([this.query, this.sort]);
    this.page$ = param$.pipe(
      switchMap(([query, sort]) =>
        this.pageNumber.pipe(
          startWith(0),
          concatMap((page) =>
            this.endpoint({ page, sort, size: this.pageSize }, query).pipe(
              indicate(this.loading)
            )
          ),
          scan<Page<T>>((state, curr) => {
            return {
              ...state,
              content: [...state.content, ...curr.content],
              number: curr.number,
              size: curr.size,
              totalElements: curr.totalElements,
            };
          })
        )
      ),
      share()
    );
  }

  sortBy(sort: Partial<Sort<T>>): void {
    const lastSort = this.sort.getValue();
    const nextSort = { ...lastSort, ...sort };
    this.sort.next(nextSort);
  }

  queryBy(query: Partial<Q>): void {
    const lastQuery = this.query.getValue();
    const nextQuery = { ...lastQuery, ...query };
    this.query.next(nextQuery);
  }

  fetch(page: number): void {
    this.pageNumber.next(page);
  }

  connect(): Observable<T[]> {
    return this.page$.pipe(map((page) => page.content));
  }

  disconnect(): void {}
}

type Query = { searchedTerm: string };

export class PaginatedScorollInfinitWrapper<T> extends PaginatedScorollInfinit<
  T,
  Query
> {
  constructor(
    protected endpoint: PaginatedEndpoint<T, Query>,
    public pageSize = 20
  ) {
    super(endpoint, {} as any, { searchedTerm: '' }, pageSize);
  }
}
