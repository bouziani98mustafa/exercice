import { Recipient } from './recipient';

export interface WFAction {
  objectId: string;
  name: string;
  type: string;
  recipients: Recipient[];
}
