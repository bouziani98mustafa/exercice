export interface MarketPlaceTree {
  features: any;
  id: string;
  entityRef: string;
  name: string;
  label: string;
  type: string;
  activated: boolean;
  data: any[];
  children: MarketPlaceTree[];
  dataByKey: any;
}
