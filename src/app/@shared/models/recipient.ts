export interface Recipient {
  objectId: string;
  firstname: string;
  lastname: string;
  email: string;
}
