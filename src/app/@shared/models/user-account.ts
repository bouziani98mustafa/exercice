export interface Features {
  profiles: any[];
  groups: Group[];
}

export interface Group {
  features: Features;
  id: string;
  entityRef: string;
  name: string;
  label: string;
  type: string;
  activated: boolean;
  data: any[];
  children: Group[];
  dataByKey: any;
  parentId: string;
}

export interface VersionLog {
  version: string;
  lifetime: Date;
  doNotDisplayWizard: boolean;
}

export interface VisualSign {
  url: string;
  type: string;
  version: number;
  entityRef: string;
  groupName: string;
  enableVisualSign: boolean;
}

export interface AddOns {
  tags: any[];
  timeZone: string;
  versionLog: VersionLog;
  visualSign: VisualSign;
  expiredSoon: boolean;
  langNotifKey: string;
  statusFilter: string;
  attachmentLaw: boolean;
  notificationEmail?: any;
}

export interface UserAccount {
  features: Features;
  id: string;
  firstName: string;
  lastName: string;
  phone: string;
  email: string;
  langKey: string;
  addOns: AddOns;
  createdDate: Date;
  updatedDate: Date;
  activated: boolean;
  disabled: boolean;
  lastConnection: Date;
  profiles: Features[];
  groups: Group[];
  delegates: any[];
}
