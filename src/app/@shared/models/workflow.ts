import { WFAction } from './action';

export interface Workflow {
  objectId: string;
  title: string;
  actions: WFAction[];
}
