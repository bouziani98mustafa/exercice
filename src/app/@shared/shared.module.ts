import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';


import { HeaderComponent } from './header/header.component';
import { OverviewSchemaComponent } from './lib/overview-schema/overview-schema.component';
import { FooterComponent } from './footer/footer.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { RouterModule } from '@angular/router';
import { TableComponent } from './table/table.component';
import { TestComponent } from '@shared/test/test.component';

import { HighlightSearchPipe } from '@shared/table/highlight-search.pipe';


@NgModule({
  declarations: [
    HeaderComponent,
    OverviewSchemaComponent,
    FooterComponent,
    TableComponent,
    TestComponent,
    HighlightSearchPipe,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,


    RouterModule,



  ],
  exports: [
    HeaderComponent,
    OverviewSchemaComponent,
    FooterComponent,
    TableComponent,
  ],
  entryComponents: [],
})
export class SharedModule {
  static forRoot() {
    return {
      ngModule: SharedModule,
      providers: [],
    };
  }
}
