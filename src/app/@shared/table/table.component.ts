import {  Component, ElementRef, Input, OnInit, PipeTransform } from '@angular/core';
import {row} from '@shared/table/table.model';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit{


  adres: string ;

  @Input()
  columns :string[] ;

  @Input()
  rows :row[] ;
  @Input()
  adress :string ;
  search=false ;
  @Input()
  surbriance=true;


  sliceStart: number ;
  selectedPageSize: number ;
  pageNumber = 0;
  totalPageNumber = 1
  selectedRows:row[] = [];
  sliceEnd : number ;
  hideSubRow = true ;



  constructor() { }


  ngOnInit() {
    this.verifyDataSet();
    this.totalPageNumber =  1; //  by default we  take 50 as  the minimal number to show
    this.sliceStart  = 0;
    this.selectedPageSize = 2;
    this.sliceEnd = Number(this.pageNumber*this.selectedPageSize)+Number(this.selectedPageSize);

    console.log(this.sliceStart)
    console.log(this.sliceEnd)


  }


  typeOf(variable : any){
    return  typeof variable;
  }

  verifyDataSet(){
    let  valid =  true;
    for (const key in this.rows) {
      if( this.rows[key].data.length  !== this.columns.length){
        valid = false;
      }
    }
    return valid
  }

  getSelectedRows(){
    console.error(this.selectedRows)
  }
  toggleSubData(event,subRowId: string ){
    let elementList: NodeListOf<HTMLElement>;

    elementList = document.querySelectorAll('[id='+subRowId+']');
    elementList.forEach(value => {
      if(value.classList.contains('hideSubRow')){
        value.classList.remove('hideSubRow');
        event.target.classList.remove('fa-eye-slash');
        event.target.classList.add('fa-eye');
      }else{
        value.classList.add('hideSubRow')
        event.target.classList.add('fa-eye-slash');
        event.target.classList.remove('fa-eye');
      }
    })
  }

  selectRow(rowId ,event){
    const variabl: row = this.rows.find( data => data.rowId == rowId);
    if(event.target.checked){
      this.selectedRows.push(variabl);
      console.error(variabl) ;
    }else {
      this.selectedRows = this.selectedRows.filter(item => item.rowId !== rowId);
    }
    event.stopPropagation();
  }

  selectAllRows(event){
    let elementList: NodeListOf<HTMLElement>;
    elementList = document.querySelectorAll('[id=tableResults] > li.primary-row');
    if(event.target.checked){
      elementList.forEach(value => {
        const checkboxInput = value.firstElementChild.firstElementChild.firstElementChild as HTMLInputElement;
        checkboxInput.checked = true  ;
        const event = new Event('change');
        // Dispatch it.
        checkboxInput.dispatchEvent(event);
      });
    }else{
      elementList.forEach(value => {
        const checkboxInput = value.firstElementChild.firstElementChild.firstElementChild as HTMLInputElement;
        checkboxInput.checked = false  ;
        const event = new Event('change');
        // Dispatch it.
        checkboxInput.dispatchEvent(event);
      });
    }


  }





  changePageSize(){
    if(this.selectedPageSize>0){
      this.totalPageNumber =   this.specialRoundNumber(this.rows.length , this.selectedPageSize) ;
      this.sliceStart = 0;
      this.sliceEnd = Number(this.selectedPageSize);
    }else{
      this.sliceStart = 0;
      this.sliceEnd = this.rows.length;
      this.totalPageNumber =  1 ;
    }
    this.pageNumber = 0 ;
    console.log(this.rows.length )
    console.log( this.selectedPageSize)
    console.error(this.totalPageNumber)

  }

  changePageNumber(){
    this.sliceStart = Number(this.pageNumber * this.selectedPageSize );
    this.sliceEnd = Number(this.pageNumber*this.selectedPageSize)+Number(this.selectedPageSize);
  }

  /**
   * this function return the result of the division (integer part + 1 if we have a decimal number )
   *
   * if a/b = 1.1 it will trun it to 2
   *
   * */
  specialRoundNumber(a , b){
    if (a%b === 0){
      return a/b
    } else {
      return  Math.floor(a/b)+1
    }
  }





  Surbrianc(event){
    let elementList: NodeListOf<HTMLElement>;
    elementList = document.querySelectorAll('[id=tableResults] > li.primary-row ' );
    elementList.forEach(value => {
      console.log(value.children.item(4).innerHTML);
      if(value.children.item(4).innerHTML.indexOf(this.adress)!==-1){
        value.children.item(4).classList.add('highlight') }
      /* let index=  value.children.item(4).innerHTML.indexOf(this.adress)
       if(index >=0){
         let inner =value.children.item(4).innerHTML;
        let index = value.children.item(4).innerHTML.indexOf(this.adress)
         // tslint:disable-next-line:max-line-length
         inner= value.children.item(4).innerHTML+'<span class=\'highlight\'>' + inner.substring(index,index+this.adress.length) + '</span>' + inner.substring(index + this.adress.length);
        // value.children.item(4).innerHTML[0];
         value.children.item(4).innerHTML =inner }}*/

      else {
        value.children.item(4).classList.remove('highlight')
      }
    })}

  }
