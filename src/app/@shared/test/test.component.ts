import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";



@Component({
  selector: 'app-test',
  templateUrl: 'test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  closeResult: string;

  constructor(private formBuilder: FormBuilder, private modalService: NgbModal,

              private router: Router) {}
  userForm: FormGroup;
  ngOnInit() {
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'notfoundpage',size: 'lg',backdrop:'static',
      windowClass: 'custom-class'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }


}
