import { MarketPlaceTree } from '@shared/models/marketplace-tree';
import { Action } from '@ngrx/store';

/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 */
export enum MarketPlacetTreeActionTypes {
  SET_MP_TREE = '[MarketPlacetTree] Set Mp Tree',
  SET_MP_TREE_SUCCESS = '[MarketPlacetTree] Set Mp Tree Success',
  SET_MP_TREE_FAILURE = '[MarketPlacetTree] Set Mp Tree Failure',
}

/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful
 * type checking in reducer functions.
 */
export class SetMarketplaceTree implements Action {
  readonly type = MarketPlacetTreeActionTypes.SET_MP_TREE;
}
export class SetMarketplaceTreeSuccess implements Action {
  readonly type = MarketPlacetTreeActionTypes.SET_MP_TREE_SUCCESS;

  constructor(public payload: MarketPlaceTree[]) {}
}
export class SetMarketplaceTreeFailure implements Action {
  readonly type = MarketPlacetTreeActionTypes.SET_MP_TREE_FAILURE;

  constructor(public payload: any) {}
}

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type MarketPlacetTreeActions =
  | SetMarketplaceTree
  | SetMarketplaceTreeSuccess
  | SetMarketplaceTreeFailure;
