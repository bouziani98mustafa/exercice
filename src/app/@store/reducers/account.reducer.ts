import { UserAccount } from '@shared/models/user-account';
import * as fromActions from '../actions/account.action';

export interface State extends UserAccount {}

const initialState: State = {
  activated: undefined,
  addOns: undefined,
  createdDate: undefined,
  delegates: undefined,
  disabled: undefined,
  email: undefined,
  features: undefined,
  firstName: undefined,
  groups: undefined,
  id: undefined,
  langKey: undefined,
  lastConnection: undefined,
  lastName: undefined,
  phone: undefined,
  profiles: undefined,
  updatedDate: undefined,
};

export function reducer(
  state = initialState,
  action: fromActions.AccountActions
): State {
  switch (action.type) {
    case fromActions.AccountActionTypes.SET_USER_ACCOUNT_SUCCESS: {
      const account = action.payload;
      return { ...state, ...account };
    }

    default: {
      return state;
    }
  }
}
export const getAccountActivated = (state: State) => state.activated;
export const getAccountAddons = (state: State) => state.addOns;
export const getAccountCreatedDate = (state: State) => state.createdDate;
export const getAccountDelegates = (state: State) => state.delegates;
export const getAccountDisabled = (state: State) => state.disabled;
export const getAccountEmail = (state: State) => state.email;
export const getAccountFeatures = (state: State) => state.features;
export const getAccountFirstName = (state: State) => state.firstName;
export const getAccountGroup = (state: State) => state.groups;
export const getAccountId = (state: State) => state.id;
export const getAccountLangKey = (state: State) => state.langKey;
export const getAccountLastConnection = (state: State) => state.lastConnection;
export const getAccountLastName = (state: State) => state.lastName;
export const getAccountLastPhone = (state: State) => state.phone;
export const getAccountProfiles = (state: State) => state.profiles;
export const getAccountUpdatedDate = (state: State) => state.updatedDate;
//
export const getAccountBasicInfo = (state: State) => {
  const {
    firstName,
    lastName,
    email,
    phone,
    updatedDate,
    lastConnection,
    id,
    createdDate,
    activated,
    disabled,
  } = state;
  return {
    firstName,
    lastName,
    email,
    phone,
    updatedDate,
    lastConnection,
    id,
    createdDate,
    activated,
    disabled,
  };
};
