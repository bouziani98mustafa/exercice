import { MarketPlaceTree } from '@app/@shared/models/marketplace-tree';
import * as fromMarketPlaceAction from '../actions/marketPlaceTree.action';

export interface State extends Array<MarketPlaceTree> {}

const initialState: State = [];

export function reducer(
  state = initialState,
  action: fromMarketPlaceAction.MarketPlacetTreeActions
): State {
  switch (action.type) {
    case fromMarketPlaceAction.MarketPlacetTreeActionTypes
      .SET_MP_TREE_SUCCESS: {
      const mptree = action.payload;
      return [...mptree];
    }

    default: {
      return state;
    }
  }
}
