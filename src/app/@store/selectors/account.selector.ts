import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as AccountReducer from '../reducers/account.reducer';

export const selectAccountState = createFeatureSelector<AccountReducer.State>(
  'account'
);
export const selectAccountActivated = createSelector(
  selectAccountState,
  AccountReducer.getAccountActivated
);

export const selectAccountBasicInfo = createSelector(
  selectAccountState,
  AccountReducer.getAccountBasicInfo
);

export const selectAccountAddons = createSelector(
  selectAccountState,
  AccountReducer.getAccountAddons
);

export const selectAccountGroup = createSelector(
  selectAccountState,
  AccountReducer.getAccountGroup
);
export const selectAccountFeatures = createSelector(
  selectAccountState,
  AccountReducer.getAccountFeatures
);

// get roles form profiles or groupes filtred by profile type:
export const selectAccountRoles = createSelector(
  selectAccountState,
  selectAccountFeatures,
  (_, features) => {
    return features && features.profiles.length
      ? features.profiles.map((e) => e.name)
      : features && features.groups
      ? features.groups.reduce(
          //Note:
          // sorry for this unreadable part
          // normaly we can use group.filter(r=>r.type==='profile').map(e=>e.name)
          // but it is clear that there is two loop :/
          //
          (res, { type, name }) => [
            ...res,
            ...(type === 'profile' ? [name] : []),
          ],
          []
        )
      : [];
  }
);

export const selectMarketplaceBelongToAccount = createSelector(
  selectAccountState,
  selectAccountFeatures,
  (_, freatures) => {
    if (freatures) {
      const { groups } = freatures;
      return groups && groups.length
        ? groups.reduce(
            (res, { type, name, parentId, entityRef }) =>
              Array.from(
                new Set([
                  ...res,
                  ...(type === 'marketplace'
                    ? [name]
                    : type === 'division'
                    ? [parentId && parentId.split(`@${entityRef}@`)[1]]
                    : []),
                ])
              ),
            []
          )
        : [];
    } else {
      return [];
    }
  }
);
