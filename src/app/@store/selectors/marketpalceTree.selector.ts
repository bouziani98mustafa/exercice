import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as marketplaceReducer from '../reducers/marketplace.reducer';

export const selectMarketPlaceTreeState = createFeatureSelector<marketplaceReducer.State>(
  'marketplaceTree'
);

export const simplifiedMpTree = createSelector(
  selectMarketPlaceTreeState,
  (state) => (state ? state.map((e) => e.name) : [])
);
