import { createSelector } from '@ngrx/store';
import { getRouterState } from '../reducers';

export const seletStateRouter = createSelector(
  getRouterState,
  (state) => state && state.state
);
export const selectParams = createSelector(
  seletStateRouter,
  (state) => state && state.params
);
