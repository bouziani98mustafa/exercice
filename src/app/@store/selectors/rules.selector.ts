import { createSelector } from '@ngrx/store';
import * as fromAccount from './account.selector';
import * as fromMPTree from './marketpalceTree.selector';

export const selectAuthrization = createSelector(
  fromAccount.selectAccountRoles,
  fromMPTree.simplifiedMpTree,
  fromAccount.selectMarketplaceBelongToAccount,
  (roles, availableMarketplaces, mpCurrentAccount) => ({
    roles,
    availableMarketplaces,
    mpCurrentAccount,
  })
);
