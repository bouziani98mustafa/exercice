import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HelpComponent } from './pages/help/help.component';
import { NotfoundpageComponent } from './pages/notfoundpage/notfoundpage.component';

const routes: Routes = [
  // we should keep this order for example if we want add another path {path:"xxx",...}
  // it should be above of :
  // {
  //   path: '',
  //   loadChildren: () =>
  //     import('@app/client/client.module').then((m) => m.ClientModule),
  // },
  { path: '', pathMatch: 'full', redirectTo: 'auth'},
  { path: 'help', component: HelpComponent },
  { path: 'rejou', loadChildren: () => import('./@modules/replay/replay.module').then(m => m.ReplayModule) },

  { path: 'utilisateurs', loadChildren: () => import('./@modules/users/users.module').then(m => m.UsersModule) },
  { path: 'bal', loadChildren: () => import('./@modules/mail-boxes/mail-boxes.module').then(m => m.MailBoxesModule) },
  { path: 'auth', loadChildren: () => import('@modules/auth/auth.module').then(m => m.AuthModule) },
  { path: '**', component: NotfoundpageComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {})],
  exports: [RouterModule],
})
export class AppRoutingModule {}
