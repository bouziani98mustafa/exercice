import { Component } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { I18nService } from '@app/@i18n/i18n.service';
import { AnalyticsService, Logger } from '@core';
import { environment } from '@env/environment';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
const log = new Logger('App');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(
    private swUpdate: SwUpdate,
    private modalService: NgbModal,
    private i18nService: I18nService,
    private analytics: AnalyticsService
  ) {
    this.i18nService.init('fr', ['en', 'en-US', 'fr']);
  }
  ngOnInit() {
    // service werker
    this.serviceWorkerInit();

    // Setup logger
    if (environment.production) {
      Logger.enableProductionMode();
    }

    // google analytics
    this.analytics.init();
  }

  serviceWorkerInit() {
    // service worker check if it's supported in current browser!
    if (this.swUpdate.isEnabled) {
      //notify by  new version of the application
      this.swUpdate.available.subscribe(() => {
        // reload if user comfirme using the new version
        const modalRef = this.modalService
          .open(ConfirmModal, {
            windowClass: 'modal-z-index warn',
            centered: true,
            backdropClass: 'backdrop-z-index',
            backdrop: 'static',
            keyboard: false,
          })
          .result.then((r) => {
            if (r === 'Accepter') {
              window.location.reload();
            }
          });
      });
    }
  }
}
@Component({
  selector: 'ngbd-modal-content',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">Nouvelle version !</h4>
    </div>
    <div class="modal-body">
      <p>New version available. Load New Version?</p>
    </div>
    <div class="modal-footer">
      <button
        type="button"
        class="btn btn-sm btn-ghost"
        (click)="activeModal.close('Accepter')"
      >
        accepter
      </button>
      <button
        type="button"
        class="btn btn-sm btn-danger"
        (click)="activeModal.close('Annuler')"
      >
        Annuler
      </button>
    </div>
  `,
})
export class ConfirmModal {
  constructor(public activeModal: NgbActiveModal) {}
}
