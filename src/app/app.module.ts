import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent, ConfirmModal } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@core';
import { SharedModule } from '@shared';
import { NotfoundpageComponent } from './pages/notfoundpage/notfoundpage.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '@env/environment';
import { HelpComponent } from './pages/help/help.component';
import { MetaReducer, StoreModule } from '@ngrx/store';
import { CustomSerializer, effects, reducers } from '@app/@store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {
  StoreRouterConnectingModule,
  RouterStateSerializer,
} from '@ngrx/router-store';
import { EffectsModule } from '@ngrx/effects';
import { OverlayModule } from '@angular/cdk/overlay';
import { UsersModule } from './@modules/users/users.module';
import { MailBoxesModule } from './@modules/mail-boxes/mail-boxes.module';
import { ReplayModule } from './@modules/replay/replay.module';
import { HighlightSearchPipe } from '@shared/table/highlight-search.pipe';

export const metaReducers: MetaReducer<any>[] = !environment.production
  ? []
  : [];

@NgModule({
  declarations: [

    AppComponent,
    NotfoundpageComponent,
    HelpComponent,
    ConfirmModal
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    TranslateModule.forRoot(),
    CoreModule,
    SharedModule.forRoot(),
    AppRoutingModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    EffectsModule.forRoot(effects),
    StoreRouterConnectingModule.forRoot(),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: false,
      name: 'SEL_STORE'
    }),
    ServiceWorkerModule.register('/ngsw-worker.js', {
      enabled: environment.production
    }),

    UsersModule,
    MailBoxesModule,
    ReplayModule
  ],
  providers: [{ provide: RouterStateSerializer, useClass: CustomSerializer }],
  bootstrap: [AppComponent],
  entryComponents: [ConfirmModal],
  exports: [

  ]
})
export class AppModule {}
