// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  api_url: 'https://localhost:8443/api',
  googleAnalyticsKey: 'UA-104548710-2',
  UPLOAD_DOSSIER_MASSE_URL: '/internal/file-manager/upload',
  VALIDATION_DOSSIER_MASSE_URL: '/v3/job/_execute/validationCircuitMasse',
  CREATION_DOSSIER_MASSE_URL: '/v3/job/_execute/creationCircuitMasse',
  UNZIP_DOSSIER_MASSE_URL: '/internal/file-manager/unzip',
  LIST_ALL_PATH_FOLDER_URL: '/internal/file-manager/list',
  FILE_DOWNLOAD_URL: '/internal/file-manager/download',
  hmr: true,
  /*googleAnalyticsKey: 'UA-104548710-2',
  api_url: 'https://pes.docapost-bpo.com/rct2/api',
  production: false,
  debounceTime: 500,
  debug: false,
  UPLOAD_DOSSIER_MASSE_URL: '/internal/file-manager/upload',
  VALIDATION_DOSSIER_MASSE_URL: '/v3/job/_execute/validationCircuitMasse',
  CREATION_DOSSIER_MASSE_URL: '/v3/job/_execute/creationCircuitMasse',
  UNZIP_DOSSIER_MASSE_URL: '/internal/file-manager/unzip',
  LIST_ALL_PATH_FOLDER_URL: '/internal/file-manager/list',
  FILE_DOWNLOAD_URL: '/internal/file-manager/download',*/
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
